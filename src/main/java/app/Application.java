package app;

import java.io.StringWriter;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

@Controller
@EnableAutoConfiguration
@SpringBootApplication
public class Application {

    @RequestMapping("/")
    @ResponseBody
    String render(@RequestParam(required = false, name = "name", defaultValue = "") String name) {
		Name safeName = new Name(name);

    	String template ="<!DOCTYPE html><html><body>"+
    					"<form action='/' method='post'>"+
    					"Name:<br>"+
    					"<input type='text' name='name' value=''>"+
    					"<input type='submit' value='Submit'>"+
    					"</form><h2>Welcome "+safeName.getValue()+"</h2></body></html>";


    	StringTemplateResolver resolver = new StringTemplateResolver();
    	resolver.setTemplateMode(TemplateMode.HTML);

    	TemplateEngine engine = new TemplateEngine();
    	engine.setTemplateResolver(resolver);

    	StringWriter out = new StringWriter();
    	Context context = new Context();
    	engine.process(template, context, out);
    	template = out.toString();


        return template;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        System.out.println("Sample request: curl http://localhost:8080/?name=Alice");
	}
	
	@ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody String handleException(RuntimeException e) {
        return null;
    }
}
