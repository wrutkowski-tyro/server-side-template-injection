package app;

final class Name {

    final String value;

    Name(String value) {
        if (value == null) {
            throw new RuntimeException("Name cannot be null");
        }
        if (value.length() == 0) {
            throw new RuntimeException("Name cannot be empty");
        }
        if (value.length() > 50) {
            throw new RuntimeException("Name is too long");
        }
        if (!value.matches("^[a-zA-Z]*$")) {
            throw new RuntimeException("Name is not compatible");
        }
        this.value = value;
    }

    String getValue() {
        return value;
    }
}