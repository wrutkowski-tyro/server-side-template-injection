package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;

@DisplayName("Usability unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_outputName_whenValidRequest() throws Exception {
        this.mockMvc.perform(post("/").param("name","<p th:text=\"${1337*1337}\"</p>"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string(not(containsString("1787569"))));
    }
}
