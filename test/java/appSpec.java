package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_outputName_whenValidRequest() throws Exception {
        this.mockMvc.perform(post("/").param("name","Bob"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("Bob")));
    }
}
